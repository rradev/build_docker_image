FROM vhewes/neutrinoml:pytorch1.12.1-cuda11.6-devel

MAINTAINER radi.radev@cern.ch

RUN pip install wandb timm --no-deps

RUN pip install -U git+https://github.com/radiradev/MinkowskiEngine -v --no-deps \
                --install-option="--blas_include_dirs=$CONDA_PREFIX/include" \
               --install-option="--blas=openblas" --install-option="--force_cuda"

